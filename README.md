##  Python Code instrumentation
Code instrumentation is one of the most important step in software testing procedure.
<br/>Indeed purpose of instrumentation is that we can monitor some of feature in code.
<br/>this package report the execution path and distance of input from each condition in your function.

## Requirement

- python3.8

 
## Installation

For add this package to any project:

```bash
cd {project directori}
git clone https://gitlab.com/roshan_roshan/pyinstrument
python3 -m venv env
source ./venv/bin/active  # for linux
.\venv\Scripts\activate   # for windows
pip install -e {path of package}/pyinstrument
```

## Usage

Command for using tool in bash(or cmd):
```
pyinstrument -i {input directori path that contain python file} -m {file_name}.py -f {function_name} -o {output directori path}
```

If you need to help how use package
```
pyinstrument -h 

```

**You can watch [this video](https://www.youtube.com/watch?v=GAQuL8sApEo&feature=youtu.be) to use the tool.**



## License
Distributed under [MIT](https://gitlab.com/roshan_roshan/pyinstrument/-/blob/master/LICENSE)
