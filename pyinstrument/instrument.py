import sys
import os
import inspect
import json
import argparse
from .instrumentation import instrument_extraction


def code_instrumentation(input_path, input, function, path):
    dir = os.path.dirname(os.path.realpath(__file__))
    dir = dir +'/data.dat'
    data = {}
    data['info'] = []
    data['info'].append({
    'input_path': input_path,
    'file_name': input,
    'function_name': function,
    "output_path": path})
    with open(dir, 'w') as outfile:
        json.dump(data, outfile)
    
    with open(path+'/distances.txt', 'w') as f:
        f.close()
    with open(path+'/executed_path.txt', 'w') as f:
        f.close()
    sys.path.append(input_path)
    file = input[:-3]
    exec("from {a} import {b}".format(a=file, b=function))
    source = inspect.getsource(eval("{a}".format(a=function))) 
    instrument_extraction(source, function, path)
    
def interface():
    parser = argparse.ArgumentParser(add_help=False)  
    parser.add_argument(
        '-h', '--help', 
        action='help', 
        default=argparse.SUPPRESS,
        help='Show this help message.')

    parser.add_argument(
        "-i",
        type=str,
        help="enter input directori path"
    )
    parser.add_argument(
        "-m",
        type=str,
        help="enter module name"
    )

    parser.add_argument(
        "-f",
        type=str,
        help="enter file name"
    )
    
    parser.add_argument(
        "-o",
        type=str,
        help="enter output directori path"
    )

    # all the results will be parsed by the parser and stored in args
    args = parser.parse_args()

    # if square is selected return the square, same for cube 
    if args.i and args.m and args.f and args.o :
        code_instrumentation(args.i, args.m, args.f, args.o)
    else:
        print("program need 4 argument. please look at help option.")
