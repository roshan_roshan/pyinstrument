import ast
import os
import astor
from collections import defaultdict

line_cond = defaultdict(list)
line_cond_num = defaultdict(list)


class BranchTransformer(ast.NodeTransformer):
    branch_num = 0

    def visit_FunctionDef(self, node):
        node.name = node.name + "_instrumented"
        return self.generic_visit(node)

    def visit_Compare(self, node):
        line_cond[node.lineno].append(astor.to_source(node))

        if node.ops[0] in [ast.Is, ast.IsNot, ast.In, ast.NotIn]:
            return node

        self.branch_num += 1
        line_cond_num[node.lineno].append(self.branch_num)
        return ast.Call(func=ast.Name("evaluate_condition", ast.Load()),
                        args=[ast.Num(self.branch_num), ast.Str(node.ops[0].__class__.__name__), node.left,
                              node.comparators[0]], keywords=[], starargs=None, kwargs=None)


def save_as_instrumented_python(instrumented, name, path):
    out_path = path + "/" + "{}_instrumented.py".format(name)
    evalue_dir = os.path.dirname(os.path.abspath(__file__))
    with open(out_path, "w") as file:
        file.write("import sys\n")
        file.write("sys.path.append(r'{a}')\n".format(a=evalue_dir))
        file.write("from evaluation import evaluate_condition\n")
        file.write("from Coverage import cover_decorator\n")
        file.write("\n")
        file.write("@cover_decorator\n")
        file.write("{}".format(instrumented))


def instrument_extraction(source, func_name, path):
    node = ast.parse(source)
    BranchTransformer().visit(node)
    node = ast.fix_missing_locations(node)
    save_as_instrumented_python(astor.to_source(node), func_name, path)
