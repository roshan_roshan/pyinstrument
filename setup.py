from setuptools import setup, find_packages
setup(
    name="pyinstrument",
    version="0.1",
    packages=find_packages(),

    install_requires=["astor==0.8.1"],

    author="Roshan Golmohammadi",
    author_email="roshangolmohamadi@gmail.com",
    description="This is an instrument package for python codes",
    keywords="instrument test execution-path distance",
    url="https://gitlab.com/roshan_roshan/pyinstrument",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],    
    entry_points={
    'console_scripts': [
        'pyinstrument = pyinstrument.instrument:interface',],},
    
    python_requires='>=3.8',    
)